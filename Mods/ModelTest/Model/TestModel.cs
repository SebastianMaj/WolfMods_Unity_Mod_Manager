using UnityEngine;
using System.Collections;
using WolfModding.ResourceLoader;

public class TestModel : MonoBehaviour
{
    void Start()
	{
		Debug.Log("I am woke!");
		var go = ResourceLoader.SpawnMesh("Creamer", new Vector3(0,0,0));
		go.GetComponent<Renderer>().material = ResourceLoader.LoadMaterial("Mat_Cream");

		go.AddComponent<MoveTest>();

		// Play Sound Test
		ResourceLoader.PlaySoundAtPoint("Sound1", Vector3.zero);
	}
}
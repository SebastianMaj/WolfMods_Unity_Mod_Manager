using UnityEngine;
using System.Collections;

public class MoveTest : MonoBehaviour
{
    private float moveSpeed = 5.0f;

    void Update()
    {
        var hor = Input.GetAxis("Horizontal");
        var ver = Input.GetAxis("Vertical");

        var moveVector = new Vector3(hor, 0.0f, ver);

        transform.Translate(moveVector * moveSpeed * Time.deltaTime);
    }
}
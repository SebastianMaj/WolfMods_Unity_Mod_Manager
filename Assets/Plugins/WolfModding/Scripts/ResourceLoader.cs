﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using WolfModding.JSONLoader;
using System.IO;
using System.Linq;

namespace WolfModding.ResourceLoader
{
	// Initial
	public partial class ResourceLoader : MonoBehaviour
	{
		// Singleton
		public static ResourceLoader Instance;

		// Awake
		private void Awake()
		{
			if (Instance != null && Instance != this)
				Destroy(gameObject);

			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

	// Mesh Loader
	public partial class ResourceLoader : MonoBehaviour
	{
		// Load into gameobject
		public static GameObject SpawnMesh(string MeshName, Vector3 pos)
		{
			// Load Json
			var currentJson = JsonLoader.Instance.getJSONByName(MeshName);

			// Found Json
			if (currentJson != null)
			{
				// Check if Model for sure
				if (currentJson.JsonContent["type"].str == "Model")
				{
					// Load mesh
					Mesh myMesh = FastObjImporter.Instance.ImportFile(currentJson.DirectoryLocation + currentJson.JsonContent["mesh"].str);
					
					// Create Gameobject
					var go = new GameObject(MeshName);
					go.transform.position = pos;
					go.AddComponent<MeshRenderer>();
					var renderer = go.AddComponent<MeshFilter>();
					renderer.mesh = myMesh;

					// Return Gameobject
					return go;
				}
				// If the json is not for a model
				else
					Debug.LogError("The given Json object is not a type model");
			}
			// json null
			else
				Debug.LogError("Could not load Json data for mesh " + MeshName);

			return null;
		}
	}

	// Material Loader
	public partial class ResourceLoader : MonoBehaviour
	{
		// Load Material Json data by name
		public static Material LoadMaterial(string MaterialName)
		{
			// Load Json
			var currentJson = JsonLoader.Instance.getJSONByName(MaterialName);

			// Found Json
			if(currentJson != null)
			{
				// Check if Material for sure
				if(currentJson.JsonContent["type"].str == "Material")
				{
					// Load the Shader
					var shader = Shader.Find(currentJson.JsonContent["shader"].str);

					// Shader is null
					if(shader == null)
					{
						Debug.LogError(string.Format("Shader {0} could not be found.", shader.name));
						return null;
					}

					// Create the material with the shader
					var newMat = new Material(shader);

					// Check if color
					if(currentJson.JsonContent.HasField("color"))
					{
						// Split string into array
						string[] colorString = currentJson.JsonContent["color"].str.Split(',');

						// Get color variables
						var r = float.Parse(colorString[0]);
						var g = float.Parse(colorString[1]);
						var b = float.Parse(colorString[2]);
						float a = 255.0f;
						if(colorString.Length == 4)
							a = float.Parse(colorString[3]); 
						
						// Create Color
						var color = new Color(r,g,b,a);
						newMat.color = color;
					}

					// Load the albedo texture
					if(currentJson.JsonContent.HasField("texture"))
					{
						// Load the file
						var texture = LoadImage(currentJson.DirectoryLocation + currentJson.JsonContent["texture"].str);

						// Apply as main texture
						newMat.mainTexture = texture; 
					}	

					// Load the normal texture
					LoadTextureIntoMaterial(currentJson.JsonContent, currentJson.DirectoryLocation, "normal", "_BumpMap", newMat);
					// Load Metallic texture _MetallicGlossMap
					LoadTextureIntoMaterial(currentJson.JsonContent, currentJson.DirectoryLocation, "metallic", "_MetallicGlossMap", newMat);
					// Load Height Map texture _ParallaxMap
					LoadTextureIntoMaterial(currentJson.JsonContent, currentJson.DirectoryLocation, "height", "_ParallaxMap", newMat);
					// Load Occlusion Map texture _OcclusionMap
					LoadTextureIntoMaterial(currentJson.JsonContent, currentJson.DirectoryLocation, "occlusion", "_OcclusionMap", newMat);
					// Load Emission Map texture _EmissionMap
					LoadTextureIntoMaterial(currentJson.JsonContent, currentJson.DirectoryLocation, "emission", "_EmissionMap", newMat);
					// Load Detail Map _DetailMask
					LoadTextureIntoMaterial(currentJson.JsonContent, currentJson.DirectoryLocation, "detailmask", "_DetailMask", newMat);

					// Return the material named
					newMat.name = MaterialName;
					return newMat;
				}
				// If the json is not a material
				else
				{
					Debug.LogError("The json loaded is not a material file.");
				}
			}
			// Json null
			else
			{
				Debug.LogError("Could not load Json data for material " + MaterialName);
			}

			return null;
		}

		// Load Texture into material
		private static void LoadTextureIntoMaterial(JSONObject _json, string jsonPath, string jsonKey, string materialKey, Material mat)
		{
			// Load the normal texture
			if(_json.HasField(jsonKey))
			{
				// Load the file
				var texture = LoadImage(jsonPath + _json[jsonKey].str);

				// Apply as main texture
				mat.SetTexture(materialKey, texture); 
			}
		}

		// Texture loader
		public static Texture2D LoadImage(string filePath) 
		{
			Texture2D tex = null;
			byte[] fileData;
		
			if (File.Exists(filePath))     
			{
				fileData = File.ReadAllBytes(filePath);
				tex = new Texture2D(2, 2);
				tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
			}
			return tex;
		}
	}

	// Sound Loader
	public partial class ResourceLoader : MonoBehaviour
	{
		// Audio loader
		public static AudioClip LoadAudio(string soundName)
		{
			// Get Audio from cache
			var curAudio = JsonLoader.Instance.AudioCache.Where(x => x.Key == soundName).SingleOrDefault().Value;

			// Return the audio
			return curAudio;
		}

		// Play Clip at point
		public static void PlaySoundAtPoint(string soundName, Vector3 pos, float volume = 1.0f)
		{
			// Get Audio
			var curAudio = LoadAudio(soundName);

			// Play Audio if not null
			if(curAudio != null)
				PlayClipAtPoint(curAudio, pos, volume);
		}

		// Extension: Audio source
		public static AudioSource PlayClipAtPoint(AudioClip clip, Vector3 point, float volume = 1.0f)
		{
			// Create object
			var go = new GameObject("Sound(" + clip.name + ")");
			
			// Add Audiosource
			var audioSource = go.AddComponent<AudioSource>();

			// Configure Audiosource
			audioSource.clip = clip;
			audioSource.volume = volume;
			go.transform.position = point;

			// Play Sound And Destory
			audioSource.Play();
			//Destroy(go, clip.length);

			return audioSource;
		}
	}
}

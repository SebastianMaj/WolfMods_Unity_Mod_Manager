﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenuAttribute(fileName = "Mod Info", menuName = "Modding/ModInfo", order = 1)]
public class ModInfo : ScriptableObject 
{
	public string modName;
    public string modAuthor;
    [TextAreaAttribute(15,20)]
    public string modDescription;
    [HideInInspector]
    public string modPath;
}

﻿using UnityEngine;
using System.Collections;
using MoonSharp.Interpreter;
using WolfModding.ModLoader;
using WolfModding.ResourceLoader;

public class LuaScript : MonoBehaviour {
    public Script script;
    public string scriptCode;

    DynValue position;

    public void Init(string scriptCode)
    {
        this.scriptCode = scriptCode;

        //UserData.RegisterAssembly();
        script = new Script();

        UserData.RegisterType<GameObject>();
        UserData.RegisterType<Debug>();
        UserData.RegisterType<Input>();
        UserData.RegisterType<GUI>();
        UserData.RegisterType<Resources>();
        UserData.RegisterType<Transform>();
        UserData.RegisterType<Vector2>();
        UserData.RegisterType<Vector3>();
        UserData.RegisterType<Vector4>();
        UserData.RegisterType<Rect>();
        UserData.RegisterType<Time>();
        UserData.RegisterType<KeyCode>();
        UserData.RegisterType<GUILayout>();
		UserData.RegisterType<LuaAddons>();
		UserData.RegisterType<LuaScript>();

		script.Globals["WolfEngine"] = new LuaAddons();
		script.Globals["LuaScript"] = UserData.CreateStatic(typeof(LuaScript));
        script.Globals["GameObject"] = UserData.CreateStatic(typeof(GameObject));
        script.Globals["Debug"] = UserData.CreateStatic(typeof(Debug));
        script.Globals["Input"] = UserData.CreateStatic(typeof(Input));
        script.Globals["KeyCode"] = UserData.CreateStatic<KeyCode>();
        script.Globals["Resources"] = UserData.CreateStatic(typeof(Resources));
        script.Globals["GUI"] = UserData.CreateStatic(typeof(GUI));
        script.Globals["GUILayout"] = UserData.CreateStatic(typeof(GUILayout));
        script.Globals["Vector2"] = UserData.CreateStatic(typeof(Vector2));
        script.Globals["Vector3"] = UserData.CreateStatic(typeof(Vector3));
        script.Globals["Vector4"] = UserData.CreateStatic(typeof(Vector4));
        script.Globals["Rect"] = UserData.CreateStatic(typeof(Rect));
        script.Globals["Time"] = UserData.CreateStatic(typeof(Time));
        script.Globals.Set("transform", UserData.Create(transform));
    }

    public IEnumerator Start()
    {
	    while (!ModLoader.Instance.DoneLoading)
		    yield return null;
		    
	    script.DoString(scriptCode);
	    
		if(scriptCode.Contains("Start"))
			script.Call(script.Globals.Get("Start"));
    }
	
    public void Update()
    {
		if(scriptCode.Contains("Update"))
        	script.Call(script.Globals.Get("Update"));
    }

    public void LateUpdate()
    {
		if(scriptCode.Contains("LateUpdate"))
        	script.Call(script.Globals.Get("LateUpdate"));
    }

    public void OnGUI()
    {
        if(scriptCode.Contains("OnGUI"))
        	script.Call(script.Globals.Get("OnGUI"));
    }
}

class LuaAddons
{
	public GameObject CreateCube(string name)
	{
		GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
		go.name = name;
		return go;
	}

	public LuaScript loadLUA(string filePathFromModFolder)
	{
		string path = "";

		#if (UNITY_EDITOR)
			path = @".\Assets\Mods\";
		#else
			path = @".\Mods\";
		#endif

		path += filePathFromModFolder;

		string sourceCode = System.IO.File.ReadAllText(path);

		GameObject go = new GameObject();
		go.name = "LuaScriptRuntime-"+Random.Range(0,9999999999).ToString();
		LuaScript luaScript = go.AddComponent<LuaScript>();
		luaScript.Init(sourceCode);

		return luaScript;
	}

	public void SpawnMesh(string name, Vector3 pos)
	{
		ResourceLoader.SpawnMesh(name, pos);
	}

	public void exec(LuaScript script, string function)
	{
		script.script.Call(script.script.Globals.Get(function));
	}
}

﻿using System.Collections;
using UnityEngine;
using UnityEditor;

public class assetBuilder : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

public class CreateAssetBundles
{
	[MenuItem("WolfTechGames/Modding/Build ResourceBundles Windows")]
	static void BuildAllAssetBundlesWindows()
	{
		BuildPipeline.BuildAssetBundles("Assets/AssetBundles", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
	}
}

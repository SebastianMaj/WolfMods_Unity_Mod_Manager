﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using System.IO;
using System.Linq;

namespace WolfModding.JSONLoader
{
	public class JsonLoader : MonoBehaviour
	{
		public static JsonLoader Instance;
		
		// Dictionary
		public Dictionary<string, List<JsonType>> JsonDictionary = new Dictionary<string, List<JsonType>>();
		public Dictionary<string, string> JsonTypeLink = new Dictionary<string, string>();

		// Audio Cache
		public Dictionary<string, AudioClip> AudioCache = new Dictionary<string, AudioClip>();

		// Awake
		private void Awake()
		{
			if (Instance != null)
				Destroy(gameObject);

			Instance = this;
			DontDestroyOnLoad(gameObject);
		}

		// Dictionary Functions
		public void AddNewJsonType(string filePath)
		{
			// Read JSON
			var fileContent = File.ReadAllText(filePath);
			Debug.Log("Adding: " + fileContent);
			var json = new JSONObject(fileContent);
			
			// Don't even bother loading
			if(json.Count <= 0)
				return;

			try
			{
				// Scan json array
				for(int i = 0; i < json.list.Count; i++)
				{
					string keyName = json.keys[i];
					if(json[keyName].HasField("type") && json[keyName]["type"].str != "info")
					{
						AddJsonObject(json[keyName], keyName, filePath);
					}
				}
			}
			catch (Exception e)
			{
				Debug.LogError("JSON issue: " + e);
			}
		}

		// Add JsonType
		private void AddJsonObject(JSONObject json, string jsonName, string filePath)
		{
				// Turn JSONObject to Json Type
				var newType = new JsonType(json["type"].str, jsonName, filePath, json);
				
				// If key doesn't exist crete new entry.
				if (!JsonDictionary.ContainsKey(newType.Type))
					JsonDictionary.Add(newType.Type, new List<JsonType>());
				// Add value to list
				JsonDictionary[newType.Type].Add(newType);
				
				// Add JSON to linker
				JsonTypeLink.Add(newType.Name, newType.Type);

				// Process the cache
				ProcessCache(newType);
				
				// Debug
				Debug.Log(string.Format("Added {0} of type {1}.", newType.Name, newType.Type));
		}

		// Cache Data
		private void ProcessCache(JsonType json)
		{
			// Go through json
			switch(json.Type)
			{
				// If json is sound
				case "Sound":
					// Load audio using WWW class
					WWW www = new WWW(json.JsonContent["sound"].str);

					// Check if 3d/2d
					bool audioThreeD = !json.JsonContent.HasField("3D") || json.JsonContent["3D"].b;

					// Attempt to add it into the dictionary
					AudioCache.Add(json.Name, www.GetAudioClip(audioThreeD));

					break;
			}
		}
		
		// Get JSONObject by name
		public JsonType getJSONByName(string name)
		{
			var singleOrDefault = JsonDictionary[JsonTypeLink[name]].SingleOrDefault(x => x.Name == name);
			if (singleOrDefault != null) return singleOrDefault;
			return null;
		}
		
		// Debug JSON File
		public void debugJSON(JSONObject obj){
			switch(obj.type){
				case JSONObject.Type.OBJECT:
					for(int i = 0; i < obj.list.Count; i++){
						string key = obj.keys[i];
						JSONObject j = obj.list[i];
						Debug.Log(key);
						debugJSON(j);
					}
					break;
				case JSONObject.Type.ARRAY:
					foreach(JSONObject j in obj.list){
						debugJSON(j);
					}
					break;
				case JSONObject.Type.STRING:
					Debug.Log(obj.str);
					break;
				case JSONObject.Type.NUMBER:
					Debug.Log(obj.n);
					break;
				case JSONObject.Type.BOOL:
					Debug.Log(obj.b);
					break;
				case JSONObject.Type.NULL:
					Debug.Log("NULL");
					break;
			}
		}
	}
}

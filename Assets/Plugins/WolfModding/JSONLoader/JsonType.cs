﻿using System;
using System.IO;
using System.Net;
using UnityEngine;

namespace WolfModding
{
    [Serializable]
    public class JsonType
    {
        // Base Properties
        public string Type;
        public string Name;
        public string FileLocation;
        public string DirectoryLocation;
        public JSONObject JsonContent;
        
        // Constructors
        public JsonType(string type, string name, string fileLocation, JSONObject _json)
        {
            Type = type;
            Name = name;
            FileLocation = fileLocation;
            DirectoryLocation = Path.GetDirectoryName(fileLocation) + "/";
            JsonContent = _json;
        }
    }
}
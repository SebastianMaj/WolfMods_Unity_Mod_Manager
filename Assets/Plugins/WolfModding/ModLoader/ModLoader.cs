﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using WolfModding.JSONLoader;

namespace WolfModding.ModLoader
{
    public class ModLoader : MonoBehaviour
    {
        // Singleton
        public static ModLoader Instance;
        // Loading
        private bool _doneLoading = false;
        public bool DoneLoading
        {
            get { return _doneLoading; }
        }

        // Debug Option
        public bool debugOption = true;
        
        [Space]
        
        // Settings
        public bool LoadInBackground = true;
        public bool DoStream = false;

        [Space]

        // List Of Mods
        public List<Mod> loadedMods = new List<Mod>();
        protected string modLocation;
        List<string> loadedCsharp = new List<string>();

        [Space]

        // Scene Black List
        public string[] SceneBlackList;

        DeferredSynchronizeInvoke synchronizedInvoke;
        CSharpCompiler.ScriptBundleLoader loader;

        void Awake()
        {
            if(Instance)
            {
                Destroy(gameObject);
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        void Start()
        {
            // Location Grabber
            string rootLocation = Path.GetFullPath(".");
            modLocation = rootLocation + "/Mods/";

            // Check if Mod Directory exists
            bool isModDirectory = Directory.Exists(modLocation);
            if(!isModDirectory)
            {
                Directory.CreateDirectory(modLocation);
                isModDirectory = true;
            }

            synchronizedInvoke = new DeferredSynchronizeInvoke(); 
 
            loader = new CSharpCompiler.ScriptBundleLoader(synchronizedInvoke); 
            //loader.logWriter = new UnityLogTextWriter(); 
            loader.createInstance = (Type t) => 
            { 
                if (typeof(Component).IsAssignableFrom(t))  
                { 
                    GameObject go = new GameObject(); 
                    go.name = t.FullName; 
                    return go.AddComponent(t); 
                } 
                else return System.Activator.CreateInstance(t); 
            }; 
            loader.destroyInstance = (object instance) => 
            { 
                if (instance is Component) Destroy(instance as Component); 
            }; 

            // Load Mods
            loadMods();
        }

        void Update()
        {
            synchronizedInvoke.ProcessQueue();
        }

        // Load All Mods
        protected void loadMods()
        {
            // Get Mod Folders
            var modFolders = Directory.GetDirectories(modLocation);

            // Run through mod folders
            foreach(var dirPath in modFolders)
            {
                // Check If Mod
                var jsonFiles = Directory.GetFiles(dirPath + "/", "*.json");
                if (jsonFiles.Length <= 0) continue;
                foreach (var filePath in jsonFiles)
                {
                    var tempPath = filePath.Replace("\\", "/");
                    var fileContent = File.ReadAllText(tempPath);
                    
                    var jsonObject = new JSONObject(fileContent);

                    try
                    {
                        var modInfoFields = new[]{ "type", "name", "author", "description" };
                        if (jsonObject.HasFields(modInfoFields) && jsonObject["type"].str == "info")
                        {
                            var modInfo = ScriptableObject.CreateInstance<ModInfo>();
                            modInfo.modName = jsonObject["name"].str;
                            modInfo.modAuthor = jsonObject["author"].str;
                            modInfo.modDescription = jsonObject["description"].str;
                            modInfo.modPath = dirPath;
                            
                            Debug.Log("[Mod] Loaded " + modInfo.modName);
                            
                            var mod = new Mod(modInfo);
                            loadedMods.Add(mod);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Could not load JSON " + e);
                    }
                }
            }

            runMods();
        }

        // Run Mods
        protected void runMods()
        {
            // Go through all the files and Mods
            foreach(var mod in loadedMods)
            {
                var files = Directory.GetFiles(mod.thisModsInfo.modPath, "*", SearchOption.AllDirectories);
                foreach(var file in files)
                {
                    if(file.EndsWith(".lua"))
                    {
                        string content = File.ReadAllText(file);
                        
                        var newLuaObj = new GameObject {name = mod.thisModsInfo.modName};
                        var luaScript = newLuaObj.AddComponent<LuaScript>();
                        luaScript.Init(content);
                    }
                    else if(file.EndsWith(".cs"))
                    {
                        if(!loadedCsharp.Contains(file))
                        {
                            string content = File.ReadAllText(file);
                            
                            CodeExecution(content, file);
                        }
                    }
                    else if (file.EndsWith(".json"))
                    {
                        JsonLoader.Instance.AddNewJsonType(file);   
                    }
                }
            }

            // Load C# Scripts into objects 
            string[] scripts = new string[loadedCsharp.Count]; 
            for(int i = 0; i < loadedCsharp.Count; i ++) 
            { 
                scripts[i] = loadedCsharp[i]; 
            } 
            loader.LoadAndWatchScriptsBundle(scripts); 

            // Done Loading
            _doneLoading = true;
        }

        // Client Execution
        public void CodeExecution(string code, string file)
        {
            // For security purposes, don't allow reading of files or execution of more code
            if (code.IndexOf("Directory") != -1) return;
            if (code.IndexOf("File") != -1) return;
            if (code.IndexOf("Reader") != -1) return;
            if (code.IndexOf("Stream") != -1) return;
            if (code.IndexOf("Assembly") != -1) return;
            if (code.IndexOf("Evaluator") != -1) return;
            if (code.IndexOf("Evaluate") != -1) return;
            if (code.IndexOf("SetAdmin") != -1) return;
            if (code.IndexOf("BeginSend") != -1) return;
            if (code.IndexOf("EndSend") != -1) return;
            if (code.IndexOf("BeginPacket") != -1) return;
            if (code.IndexOf("EndPacket") != -1) return;
            
            loadedCsharp.Add(file);
        }

        // Returns
        public Mod getModByName(string modName)
        {
            return loadedMods.FirstOrDefault(curMod => curMod.modName == modName);
        }

        /// <summary>
        /// On GUI for debug
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private void OnGUI()
        {
            if (debugOption)
            {
                GUILayout.Label("Mods Loaded: " + loadedMods.Count);
            }
        }
    }
}
﻿namespace WolfModding.ModLoader
{
    [System.Serializable]
    public class Mod
    {
        public string modName;
        public ModInfo thisModsInfo;

        public Mod(ModInfo newInfo)
        {
            modName = newInfo.modName;
            thisModsInfo = newInfo;
        }
    }
}